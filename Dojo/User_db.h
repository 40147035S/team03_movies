#ifndef USER_DB_H_INCLUDED
#define USER_DB_H_INCLUDED

#include <vector>
#include "User.h"
#include "MyLoveList.h"

//用戶資料庫
class User_db
{
public:
//建構子
	User_db() = default;
	User_db(const std::vector<user> &tmp)
		:us(tmp), User_Num(tmp.size()){}
	User_db(const user &u) {
		us.push_back(u);
		User_Num = 1;
	}
	User_db(const User_db &ua) :User_db(ua.us) {}

//資料庫變更
	//新增一名用戶
	bool Add(const user &u);
	//刪除一名用戶
	bool delete_(const std::string &name);
	
	//改動一名用戶
	bool Fix_User_password(const std::string &name, const std::string &old_password, const std::string &new_password);
	bool Fix_User_name(const std::string &old_name,const std::string &pass,const std::string &new_name);
	//資料庫匯出
	bool Save_UserDb(const std::string &filename);
	//資料匯入
	bool Load_UserDb(const std::string &filename);
	//初始化資料庫
	void ResetDatabase();
//資料庫管理
	//資料庫資料筆數
	int GetNumOfUser()const;
	//資料庫排序
	void UserSort();
	
//用戶管理
	//查找一名用戶(以暱稱)
	int Search(const std::string &name)const;
	//查找一名用戶(以用戶資料)
	int Search(const user & U1)const;
	//用戶上線狀態查找 (以暱稱)
	bool is_online(const std::string name) const;
	//用戶登入
	bool Log_in(const std::string &Ac, const std::string &Pa);
	//用戶登出
	void Log_out(const user &tmp);

private:
	//用戶們
	std::vector <user> us;
	//用戶總數
	int User_Num = 0;

};

#endif

/*
請務必注意:寫函式時請先以中文註明功能，並將函式的實作部分
寫進.cpp中，以方便閱讀
*/

//開發方向
/*
函式新增:
	1. 資料庫匯入/出
	2. 改動一個現存用戶
	3. 我的最愛列表的加入(函式修改)
*/
/*
Refactor方向:
	1. 資料庫排序可否依使用者的需要來做特定排序? (依帳號/暱稱)?
	2. Search在以暱稱搜尋時，是否該列出database中所有有相同暱稱之用戶?
*/