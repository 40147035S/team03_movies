#include "gtest/gtest.h"
#include <string>
#include <algorithm>
#include "movie.h"
#include "movie_db.h"
#include "User.h"
#include "User_db.h"
#include "Date.h"
using namespace std;

class MyTest : public ::testing::Test
{
};


//the movie test
TEST_F(MyTest, Add_A_MOVIE)
{
	DB_movie D1;
	Movie M1("Hello World");
	D1.InputMovie(M1);
	EXPECT_EQ(1, D1.GetNumOfMovie());

}
TEST_F(MyTest, Add_SOME_MOVIE)
{
	DB_movie D2;

	Movie M2("The Hunger Games: Mockingjay - Part ��");
	Movie M3("Theory of Everything");
	Movie M4("Love, Rosie");
	Movie M5("Exodus: Gods and Kings");
	Movie M6("RISE OF THE LEGEND");

	D2.InputMovie(M2);
	D2.InputMovie(M3);
	D2.InputMovie(M4);
	D2.InputMovie(M5);
	D2.InputMovie(M6);

	EXPECT_EQ(5, D2.GetNumOfMovie());

}
TEST_F(MyTest, SEARCH_MOVIE)
{
	DB_movie D2;

	Movie M2("The Hunger Games: Mockingjay - Part ��");
	Movie M3("Theory of Everything");
	Movie M4("Love, Rosie");
	Movie M5("Exodus: Gods and Kings");
	Movie M6("RISE OF THE LEGEND");

	D2.InputMovie(M2);
	D2.InputMovie(M3);
	D2.InputMovie(M4);
	D2.InputMovie(M5);
	D2.InputMovie(M6);
	EXPECT_EQ(1, D2.Search("Love, Rosie"));
}
TEST_F(MyTest, RESET_MOVIE)
{
	DB_movie D2;

	Movie M2("The Hunger Games: Mockingjay - Part ��");
	Movie M3("Theory of Everything");
	Movie M4("Love, Rosie");
	Movie M5("Exodus: Gods and Kings");
	Movie M6("RISE OF THE LEGEND");

	D2.InputMovie(M2);
	D2.InputMovie(M3);
	D2.InputMovie(M4);
	D2.InputMovie(M5);
	D2.InputMovie(M6);
	D2.ResetDatabase();
	EXPECT_EQ(0,D2.GetNumOfMovie());
}
TEST_F(MyTest, Add_A_SCORE)
{
	DB_movie D1;
	Movie M1("Hello World");
	D1.InputMovie(M1);
	int score = 1;
	float average = D1.AddScore(0, score);
	EXPECT_EQ(1, average);
}
TEST_F(MyTest, Add_SOME_SCORE)
{
	DB_movie D1;
	Movie M1("Hello World");
	D1.InputMovie(M1);
	int s1 = 1;
	int s2 = 2;
	int s3 = 3;
	int s4 = 4;
	int s5 = 5;
	D1.AddScore(0, s1);
	D1.AddScore(0, s2);
	D1.AddScore(0, s3);
	D1.AddScore(0, s4);
	float average = D1.AddScore(0, s5);

	EXPECT_EQ(3, average);
}

//the user test
TEST_F(MyTest, Add_A_USER)
{
	user P1("ACC","PSW","1");
	User_db U1(P1);

	EXPECT_EQ(1, U1.GetNumOfUser());
}
TEST_F(MyTest, The_First_USER)
{
	User_db ua;
	user U1("001", "001", "number one");
	ua.Add(U1);
	EXPECT_EQ(1, ua.GetNumOfUser());
}

User_db The_James_Database;
TEST_F(MyTest, Hello_James)
{
	user James("James0", "James1", "James");
	The_James_Database.Add(James);

	EXPECT_EQ(1, The_James_Database.GetNumOfUser());
}
TEST_F(MyTest, Is_James_In_the_Database)
{
	EXPECT_EQ(0, The_James_Database.Search("James"));
}
TEST_F(MyTest, Is_James_Online)
{
	bool J = The_James_Database.Log_in("James0", "James1");

	EXPECT_EQ(false, The_James_Database.is_online("James"));
}
TEST_F(MyTest, ByeBye_James)
{
	The_James_Database.delete_("James");

	EXPECT_EQ(-1, The_James_Database.Search("James"));
}
TEST_F(MyTest, Reset_Movie_DataBase)
{
	user James("James0", "James1", "James");
	The_James_Database.Add(James);
	The_James_Database.ResetDatabase();

	EXPECT_EQ(0, The_James_Database.GetNumOfUser());
}

TEST_F(MyTest, User_db_Sort)
{
    User_db User_DataBase;
	user James("James0", "James1", "James");
	user Amy("Amy0", "Amy1", "Amy");
	user YOuOY("YOuOY0", "YOuOY1", "YOuOY");
	User_DataBase.Add(James);
	User_DataBase.Add(Amy);
	User_DataBase.Add(YOuOY);

	User_DataBase.UserSort();

	EXPECT_EQ(0, User_DataBase.Search("Amy"));
}


//the date test
TEST_F(MyTest, NO_Is_leap_year)
{
	Date day("20141230");
	EXPECT_EQ(false, day.Is_leap_year());
}
TEST_F(MyTest, YES_Is_leap_year)
{
	Date day("20161230");
	EXPECT_EQ(true, day.Is_leap_year());
}

TEST_F(MyTest, Sum)
{
	Date day("20140101");
	EXPECT_EQ(735600, day.Sum());
}

TEST_F(MyTest, Days_Calculator)
{
	Date day("20141230"), today("20141227");
	EXPECT_EQ(3, day.Days_Calculator(today));
}

TEST_F(MyTest, isBefore)
{
	Date day("20141230"), today("20141227");
	EXPECT_EQ(false, day.isBefore(today));
}
TEST_F(MyTest, MovieIsOnScreen)
{
	Date today("20141228"),movieday("20141227");
	Movie m("Theory of Everything", "good", movieday);
	EXPECT_EQ(true, m.IsOnScreen(today));
}
