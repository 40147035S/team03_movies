#include "User.h"
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;
//User匯入
istream & operator >> (istream &is, user &u)
{
	is >> u.account >> u._name >>  u.password;
	return is;
}
//User匯出
ostream & operator << (ostream &os, const user &u)
{
	os << u.account << " " << u._name << " " << u.password;
	os << endl;
	return os;
}

//User--------------------------------------------------

//帳戶管理
	//個人資料的輸出 : 帳號
string user::MyAccount() const
{
	return account;
}
	//個人資料的輸出 : 暱稱
string user::MyName() const
{
	return _name;
}
	//暱稱比對
bool user::same_name(const std::string name) const
{
	return (name == _name);
}
	//帳號比對
bool user::same_person(const user &U) const
{
	return (_name == U._name && account == U.account);
}
	//密碼確認
bool user::password_correct(const std::string &S) const
{
	if (password == S) return true;
	return false;
}
	//用戶比對
bool user::same_user(const user &tmp) const
{
	return (password == tmp.password && account == tmp.account && _name == tmp._name);
}
//帳戶更動
	//密碼更換
bool user::password_alter(const std::string &old, const std::string &new_)
{
	if (password == old) {
		password = new_;
		return true;
	}
	return false;
}
	//暱稱更換
bool user::name_alter(const std::string &pass, const std::string &new_)
{
	if (password == pass) {
		_name = new_;
		return true;
	}
	return false;
}
//在線狀態
	//上線與否
bool user::online_or_not() const
{
	return log_in_;
}
	//狀態切換:上線
void user::getonline()
{
	log_in_ = true;
}
	//狀態切換:下線
void user::offline()
{
	log_in_ = false;
}

