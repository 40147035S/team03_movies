#include "Date.h"

using namespace std;
//日期運算
	//日期比對
bool operator == (const Date &lhs, const Date &rhs)
{
	return (lhs.Year == rhs.Year && lhs.Month == rhs.Month && lhs.Day == rhs.Day);
}
//日期處理
	//日期指派
void Date::Date_assign(const Date &D)
{
	Year = D.Year;
	Month = D.Month;
	Day = D.Day;
}
	//字串轉日期
void Date::String_to_Date(const std::string &s) 
{
	Date tmp(s);
	this->Date_assign(tmp);
}
	//日期轉字串
string Date::Date_to_String() const 
{
	string tmp;
	Date t1(*this);
	int C = 10;
	while (t1.Year > 0) {
		tmp.push_back(t1.Year % 10);
		t1.Year /= 10;
	}
	while (t1.Month > 0) {
		tmp.push_back(t1.Month % 10);
		t1.Month /= 10;
	}
	while (t1.Day > 0) {
		tmp.push_back(t1.Day % 10);
		t1.Day /= 10;
	}
	return tmp;
}
	//是否閏年
bool Date::Is_leap_year() const
{
	return (Year % 4 == 0 && Year % 100 || Year % 400 == 0);
}
	//加日期

Date Date::operator += (int n)
{
	Date tmp(*this);
	int Day_of_each_month[2][13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, //not leap year
									 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};//leap year
	int y = this->Is_leap_year();
	for (int i = 0; i < n; i++)
		{
			//if (day)要一天一天加還沒寫完...
		}

	return tmp;
}

//日期計數
const int Date::Sum()const
{
	int md[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int count = Year * 365; //幾年了
	count += (Year - 1) / 4 + 1; //四年一次潤年+1
	count -= (Year - 1) / 100 + 1; //百年不潤
	count += (Year - 1) / 400 + 1; //四百潤

	for (int i = 1; i < Month; ++i) count += md[i]; //月
	if (Month > 2 && this->Is_leap_year()) ++count; //閏年且超過2月

	count += Day; //日
	return count;
}

//日期相差
const int Date::Days_Calculator(const Date &D1)const
{
	return this->Sum() - D1.Sum();
}
//日期前後
bool Date::isBefore(const Date &D1)const
{
	if (this->Days_Calculator(D1) < 0) return true;
	else return false;
}