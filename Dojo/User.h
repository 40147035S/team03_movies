#ifndef USER_H_INCLUDED
#define USER_H_INCLUDED

#include <string>
#include <vector>
#include "MyLoveList.h"

//用戶
class user 
{
	//User匯入
	friend std::istream & operator >> (std::istream &is, user &u);
	//User匯出
	friend std::ostream & operator << (std::ostream &os, const user &u);
public:
//建構子
	user() = default;
	user(std::string A1, std::string P1, std::string N1)
		:account(A1), password(P1), _name(N1) {}
	user(const user &tmp) 
		:user(tmp.account, tmp.password, tmp._name) {}
	const std::string Name(){return _name;}
//帳戶管理
	//個人資料的輸出 : 帳號
	std::string MyAccount() const;
	//個人資料的輸出 : 暱稱
	std::string user::MyName() const;
	//暱稱比對
	bool same_name(const std::string name) const;
	//帳號比對
	bool same_person(const user &U) const;
	//密碼確認
	bool password_correct(const std::string &S) const;
	//用戶比對
	bool same_user(const user &tmp) const;
//帳戶更動
	//密碼更換
	bool password_alter(const std::string &old, const std::string &new_);
	//暱稱更換
	bool name_alter(const std::string &pass, const std::string &new_);
//在線狀態
	//上線與否
	bool online_or_not() const;
	//狀態切換:上線
	void getonline();
	//狀態切換:下線
	void offline();

private:
	//登入狀態
	bool log_in_ = false;
	//個人資料 : 帳號，密碼，暱稱
	std::string account,password,_name;
	//我的最愛列表
	MyLoveList ML;
};
#endif

/*
請務必注意:寫函式時請先以中文註明功能，並將函式的實作部分
寫進.cpp中，以方便閱讀
*/

//開發方向
/*
函式新增:
	1. 我的最愛
	2. 尊貴會員?
*/
/*
Refactor方向:
	
*/