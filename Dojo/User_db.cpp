#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include "User_db.h"

using namespace std;

//sort (friend function)
bool SortFunction(const user &i, const user &j)
{
	return (i.MyAccount() < j.MyAccount());
}

//User_db-------------------------------------------------
//資料庫變更
	//新增一名用戶
bool User_db::Add(const user &u)
{
	for (int i = 0; i < User_Num; i++) {
		if (us[i].same_user(u)) return false;
	}
	us.push_back(u);
	User_Num++;
	UserSort();

	return true;
}
	//刪除一名用戶
bool User_db::delete_(const string &name)
{
	int tmp = Search(name);
	if (tmp >= 0) {
		us.erase(us.begin() + tmp);
		User_Num--;
		return true;
	}
	return false;
}
	//改動一名用戶
		//密碼
bool User_db::Fix_User_password(const string &name, const string &old_password,const string &new_password)
{
	int id=	Search(name);
	return us[id].password_alter(old_password, new_password);
}
		//暱稱
bool User_db::Fix_User_name(const string &old_name, const string &pass, const string &new_name)
{
	int id = Search(old_name);
	return us[id].name_alter(pass, new_name);
}
	//資料庫匯出
bool User_db::Save_UserDb(const string &filename)
{
	ofstream ofile(filename);
	if (!ofile)
	{
		cout << "Wrong file name" << endl;
		return false;
	}
	ofile << User_Num << endl;
	for (int i = 0; i<User_Num; i = i + 1)
	{
		ofile << us[i];
	}
	return true;
}
	//資料匯入
bool User_db::Load_UserDb(const string &filename)
{
	ifstream ifile(filename);
	if (!ifile)
	{
		cout << "Wrong file name" << endl;
		return false;
	}
	ifile >> User_Num;
	for (int i = 0; i<User_Num; i = i + 1)
	{
		ifile >> us[i];
	}
	return true;
}
	//初始化資料庫
void User_db::ResetDatabase()
{
	us.clear();
	User_Num = 0;
}
//資料庫管理
	//資料庫資料筆數
int  User_db::GetNumOfUser() const
{
	return User_Num;
}
	//資料庫排序
void User_db::UserSort()
{
	sort(us.begin(), us.end(), SortFunction);
}
//用戶管理
	//查找一名用戶(以暱稱)
int  User_db::Search(const string &name) const
{
	User_db temp = *this;
	for (int i = 0; i < User_Num; i++)
	{
		if (temp.us[i].Name().compare(name) == 0) return i;
	}
	return -1;
}
	//查找一名用戶(以用戶資料)
int  User_db::Search(const user & U1) const
{
	for (int i = 0; i < User_Num; i++) {
		if (us[i].same_user(U1)) return i;
	}
	return -1;
}
	//用戶上線狀態查找 (以暱稱)
bool User_db::is_online(const string name) const
{
	for (int i = 0; i < User_Num; i++) {
		if (us[i].same_name(name)) {
			return (us[i].online_or_not());
		}
	}
	return false;
}
	//用戶登入
bool User_db::Log_in(const string &Ac, const string &Pa)
{
	int SrIndex = Search(Ac);

	//in the database
	if (SrIndex != -1)
	{
		if (this->us[SrIndex].password_correct(Pa))
		{
			this->us[SrIndex].getonline();
			//login success
			return true;
		}
		//login fail
		return false;
	}
	//not in the database
	return false;
}
	//用戶登出
void User_db::Log_out(const user &tmp)
{
	int SrIndex = Search(tmp);
	if (SrIndex >= 0) {
		us[SrIndex].offline();
	}
}
