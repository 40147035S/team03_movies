#ifndef MYLOVELIST_H_INCLUDED
#define MYLOVELIST_H_INCLUDED

#include "movie.h"
#include <vector>

class MyLoveList
{
public:
//建構子
	MyLoveList() = default;
	MyLoveList(const std::vector<Movie> &L1):LL(L1) {}
	MyLoveList(const MyLoveList &ML) :MyLoveList(ML.LL) {}

//我的最愛改動
	//新增一筆
	void Add(const Movie &M1);
//內容管理
	//回傳筆數
	int GetListNum() const;
private:
	std::vector<Movie> LL;
	int LoveListNum = 0;
};

#endif

/*
請務必注意:寫函式時請先以中文註明功能，並將函式的實作部分
寫進.cpp中，以方便閱讀
*/

//開發方向
/*
函式新增:
	1. Search (以電影名稱)
	2. Sort (以名稱，上映日期...)
	3. Deletion
*/
/*
Refactor方向:
	1. add時是否該考慮重複的情況?

*/