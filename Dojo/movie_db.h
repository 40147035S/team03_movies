﻿#ifndef DB_M_
#define DB_M_

#include<algorithm>
#include<vector>
#include"movie.h"

//電影資料庫
class DB_movie
{
public:
//建構子
	DB_movie() = default;
	DB_movie(const std::vector<Movie> &tmp)
		:M1(tmp), NumOfMovie(tmp.size()){}
	DB_movie(const Movie &u) {
		M1.push_back(u);
		NumOfMovie = 1;
	}
//資料庫變更
	//新增一部電影
	void InputMovie(const Movie &M1);
	//刪除一部電影
	bool delete_(const std::string &name);
	//資料庫匯出
	void Save(const char filename[]);
	//資料庫匯入
	bool Load(const char filename[]);
	//初始化資料庫
	void ResetDatabase();
//資料庫管理
	//資料庫資料筆數
	const int GetNumOfMovie() const;
	//資料庫排序(依名稱、評分)
	void MovieSortByName();
	void MovieSortByScore();
	
//電影管理
	//查找一部電影(以名稱)
	int Search(const std::string &name)const;
	//查找一部電影(以電影資料)
	int Search(const Movie & M1)const;
	//列出i天之內上映的電影
	void Coming_Soon(const Date &today, const int within_day);
	//列出7天之內上映的電影(呼叫Coming_Soon函式實作)
	void AboutToCome(const Date &today);
	//列出上映中的電影(呼叫Coming_Soon函式實作)
	void HitCinemas(const Date &today);
	//列出上映中電影排行榜
	void MovieTable(const Date &today);
	//增加分數
	float AddScore(int i, int score);

private:
	//電影們
	std::vector<Movie> M1;
	//電影總數
	int NumOfMovie = 0;

};
#endif // DB_M_

/*
請務必注意:寫函式時請先以中文註明功能，並將函式的實作部分
寫進.cpp中，以方便閱讀
*/

//開發方向
/*
函式新增:
	1. Search (依日期)
	2. 列出一定日期內的電影 
	EX: void About_to_Come(); //列出一個星期之內上映的電影----------完成
	EX: void on_hit (); //列出前兩個禮拜上映的電影
*/
/*
Refactor方向:
	1. 資料庫排序可否依使用者的需要來做特定排序? (依名稱/日期/評分)?

*/