#ifndef DATE_H_INCLUDED
#define DATE_H_INCLUDED
#include <string>

class Date
{
//日期運算
	//日期比較
	friend bool operator == (const Date &lhs, const Date &rhs);
public:
//建構子
	Date() = default;
	Date(const std::string &S) {
		Year = (S[0] - '0') * 1000 + (S[1] - '0') * 100 + (S[2] - '0') * 10 + (S[3] - '0') * 1;
		Month = (S[4] - '0') * 10 + (S[5] - '0') * 1;
		Day = (S[6] - '0') * 10 + (S[7] - '0') * 1;
	}
	Date(const Date &D) : Year(D.Year), Month(D.Month), Day(D.Day) {}
//日期處理
	//日期指派
	void Date_assign(const Date &D);
	//字串轉日期
	void String_to_Date(const std::string &s);
	//日期轉字串
	std::string Date_to_String() const;
	//是否閏年
	bool Is_leap_year() const;
	//加日期
	Date operator += (int n);
	//日期計數
	const int Sum()const;
	//日期相差
	const int Days_Calculator(const Date &D1)const;
	//日期前後
	bool isBefore(const Date &D1)const;
private:
	//string 格式: (年)(月)(日)
	//EX: 20141224
	int Year=0;		//年
	int Month=0;	//月
	int Day=0;		//日
};

#endif
/*
請務必注意:寫函式時請先以中文註明功能，並將函式的實作部分
寫進.cpp中，以方便閱讀
*/

//開發方向
/*
函式新增:
	1. 日期比前後 ----------完成
	EX: bool isBefore(const Date &D1);
		bool isAfter(const Date &D1);
	2. 日期差幾天?       ----------完成
	EX: int Days_Between(const Date &D1);

*/
/*
Refactor方向:
	1. Date_to_String()有沒有更漂亮的寫法?

*/