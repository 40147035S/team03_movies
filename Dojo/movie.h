#ifndef MOVIE_H_INCLUDED
#define MOVIE_H_INCLUDED

#include "Date.h"
//電影
class Movie
{
	//電影匯入
    friend std::istream & operator >> (std::istream &is, Movie &m);
    //電影匯出
	friend std::ostream & operator << (std::ostream &os,const Movie &m);
	//名稱比對
	friend bool operator == (const Movie &M1, const char name_[]);
public:
//建構子
	Movie() = default;
	Movie(const std::string &n2) :Movie() {
		name = n2;		
	}
	Movie(const std::string &n2, const std::string thedescribe, Date thedate) :Movie() {
		name = n2;
		describe = thedescribe;
		date = thedate;
	}
	Movie(const Movie & M1) :name(M1.name),describe(M1.describe)
	,date(M1.date),score(M1.score),NumOfScore(M1.NumOfScore) {}
//內容更動
	//增加用戶評分
    float AddScore(int s);

//電影管理
	//評分比較
    bool operator < (const Movie &movie)const;
	//電影比對(是否相同?)
	bool Compare_Movie(const Movie &M1)const;	
	//判斷此部電影是否為上映中
	bool IsOnScreen(const Date &today)const;
	//回傳評分
	float MyScore() const;
	//回傳上映日期
	const std::string StringTypeDate();
	//回傳上映日期(以Date形式)
	const Date DateTypeDate();
	//回傳名稱
	std::string MyName() const;
private:
    std::string name;				//電影名稱
    std::string describe;			//內容描述
	Date date;						//上映日期
    int score = 0,					//用戶評分
		NumOfScore = 0;				//評分人數			
									//平均分數
};

#endif // Movie__

/*
請務必注意:寫函式時請先以中文註明功能，並將函式的實作部分
寫進.cpp中，以方便閱讀
*/

/*
請務必注意:寫函式時請先以中文註明功能，並將函式的實作部分
寫進.cpp中，以方便閱讀
*/

//開發方向
/*
函式新增:
	1. 電影比對 (依日期/評分)
*/
/*
Refactor方向:
	1. 有沒有一個更漂亮的方法來存電影的總分?
	2. 電影比對的function真的是必要的嗎?還是能用operator代替?
*/