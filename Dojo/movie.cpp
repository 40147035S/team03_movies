#include<iostream>
#include<string>
#include<fstream>
#include"movie.h"
#define ONTHEATER 14
using namespace std;
	//電影匯入
istream & operator >> (istream &is, Movie &m)
{
	string D1;
	is >> m.name >> D1 >> m.describe;
	is >> m.score >> m.NumOfScore;
	m.date.String_to_Date(D1);
		
	return is;
}
	//電影匯出
ostream & operator << (ostream &os, const Movie &m)
{
	string S = m.date.Date_to_String();
	os << m.name << ' ' << S << ' ' << m.describe;
	os << m.score << ' ' << m.NumOfScore;
	os << endl;
	return os;
}
	//名稱比對
bool operator == (const Movie &M1, const char name_[])
{
    return ( M1.name == name_ )? true:false;
}
//內容更動
	//增加用戶評分
float Movie::AddScore(int s)
{
	score += s;
	NumOfScore++;

	return static_cast<float>(score) / NumOfScore;
}
//電影管理
	//評分比較
bool Movie::operator < (const Movie &m)const
{
	return static_cast<float>(score) / NumOfScore < static_cast<float>(m.score) / m.NumOfScore;
}
	//電影比對(是否相同?)
bool Movie::Compare_Movie(const Movie &M1)const
{
	return (name == M1.name && describe == M1.describe && date == M1.date);
}
   //判斷此部電影是否為上映中
bool Movie::IsOnScreen(const Date &today)const
{
	int day = date.Days_Calculator(today);//電影距離今天還有幾天上映
	if (day >= 0 - ONTHEATER && day <= 0) return true;
	return false;
}
	//回傳評分
float Movie::MyScore() const
{
	return static_cast<float>(score) / NumOfScore;
}
	//回傳上映日期
const string Movie::StringTypeDate()
{
	string S1= date.Date_to_String();
	return S1;
}
	//回傳上映日期(以Date形式)
const Date Movie::DateTypeDate()
{
	return date;
}
	//回傳名稱
string Movie::MyName() const
{
	return name;
}

