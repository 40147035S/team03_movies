#include<iostream>
#include<string>
#include<fstream>
#include<algorithm>
#include<vector>
#include"movie_db.h"
#include"Date.h"
#define COMINGSOON 7
using namespace std;

//sort (friend function)
//依照電影名稱排序
bool SortFunctionByName(const Movie &i, const Movie &j)
{
	return (i.MyName() < j.MyName());
}
//依照電影評分排序
bool SortFunctionByScore(const Movie &i, const Movie &j)
{
	return (i.MyScore()<j.MyScore());
}


//DB_movie-------------------------------------------------
//資料庫變更
	//新增一部電影
void DB_movie::InputMovie(const Movie &MA)
{
	M1.push_back(MA);
	NumOfMovie++;
	MovieSortByName();
}
	//刪除一部電影
bool DB_movie::delete_(const std::string &name)
{
	int tmp = Search(name);
	if (tmp >= 0) {
		M1.erase(M1.begin() + tmp);
		NumOfMovie--;
		return true;
	}
	return false;
}
//檔案:格式
/*
(總數)
(name) (date) (describe) (score) (NumOfScore)
...
*/
	//資料庫匯出
void DB_movie::Save(const char filename[])
{
	ofstream ofile(filename);
	ofile << NumOfMovie << endl;
	for (int i = 0; i<NumOfMovie; i = i + 1)
	{
		cout << M1[i];
	}
}
	//資料庫匯入
bool DB_movie::Load(const char filename[])
{
	ifstream ifile(filename);
	if (!ifile) return false;
	cin >> NumOfMovie;
	for (int i = 0; i<NumOfMovie; i = i + 1)
	{
		ifile >> M1[i];
	}
	return true;
}
	//初始化資料庫
void DB_movie::ResetDatabase()
{
	M1.clear();
	NumOfMovie = 0;
}
//資料庫管理
	//資料庫資料筆數
const int DB_movie::GetNumOfMovie() const
{
	return NumOfMovie;
}
//資料庫排序(依名稱、評分)
     //依名稱排序
void DB_movie::MovieSortByName()
{
	sort(M1.begin(), M1.end(), SortFunctionByName);
}
     //依評分排序
void DB_movie::MovieSortByScore()
{
	sort(M1.begin(), M1.end(), SortFunctionByScore);
}
//電影管理
	//查找一部電影(以名稱)
int DB_movie::Search(const std::string &name)const
{
    for(int i=0; i < NumOfMovie; i++)
    {
        if(M1[i].MyName() == name) return i;
    }
    return -1;
}
	//查找一部電影(以電影資料)
int DB_movie::Search(const Movie & MA)const
{
	for (int i = 0; i < NumOfMovie; i++)
	{
		if (M1[i].Compare_Movie(MA)) return i;
	}
	return -1;
}
	//列出幾天(within_day)之內上映的電影
void DB_movie::Coming_Soon(const Date &today, const int within_day)
{
	for (int i = 0; i < NumOfMovie; i++)
	{
		Date d = M1[i].DateTypeDate();
		int day = d.Days_Calculator(today);
		if (day > 0 && day <= within_day) cout << M1[i];
	}
}
    //列印出一周內即將上映的電影
void DB_movie::AboutToCome(const Date &today)
{
	Coming_Soon(today, COMINGSOON);
}
    //列印出正在上映的電影
void DB_movie::HitCinemas(const Date &today)
{
	for (int i = 0; i < NumOfMovie; i++)
	{
		if (M1[i].IsOnScreen(today) == true) cout << M1[i];
	}
}
    //印出電影排行榜(依評分)
void DB_movie::MovieTable(const Date &today)
{
	MovieSortByScore();
	HitCinemas(today);
}
	//增加分數
float DB_movie::AddScore(int index, int score)
{
	return M1[index].AddScore(score);
}
